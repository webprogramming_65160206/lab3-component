import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAddressBookStore = defineStore('address_book', () => {


  interface AddressBook {
    id: number
    name: string
    tel: string
    gender: string
  }
  let lastId = 1
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  })
  const isAddNew = ref(false)
  function save() {
    if (address.value.id > 0) {//Edit
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    } else { // add new
      addressList.value.push({ ...address.value, id: lastId++ })
    }
    //...x.valueเอาไว้ แยกค่าออกมาจากobject แล้ว setใหม่
    isAddNew.value = false
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }
  const addressList = ref<AddressBook[]>([])
  function edit(id: number) {
    isAddNew.value = true
    // this line findIndex need to put arrow condition for condition that you want
    // we do like this because it will find the object that has the same id first and then it will get index (because when we use array we use index not id)
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    // we use 33 line to make object to json string first and then make to JSON object
    // this is called copy object
    // we do this because if we use the addressList the table that is shown will change when we are editing
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  }
  function remove(id: number) {
    const removedIndex = addressList.value.findIndex((item) => item.id === id)
    // use splice not splice
    addressList.value.splice(removedIndex, 1)
  }
  function cancel() {
    isAddNew.value = false
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }
  return { address, addressList, save, isAddNew, cancel, edit, remove }
})
